let CoolFuncs = {
    setParametersByArray: function(json_params, json_params_names)
    {
        for (let p_name in json_params_names)
        {
            if(json_params[json_params_names[p_name]])
            {
                this[json_params_names[p_name]] = json_params[json_params_names[p_name]];
            } else
            {
                throw new Error("Have no mandatory parameter: " + json_params_names[p_name]);
            }
        }
    }
};