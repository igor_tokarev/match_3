class Shape extends PIXI.Sprite {
    constructor(shapeType, x, y, cellSize, handleShapeClick){

        super(PIXI.loader.resources["src/images/" + shapeType.name + ".png"].texture);

        this.handler = this.handler.bind(this);

        this.animationParameters = {
            to: {x:0, y:0}
        };
        this.handleShapeClick = handleShapeClick;
        this.xCoord = x;
        this.yCoord = y;
    
        this.x = this.xCoord * cellSize;
        this.y = this.yCoord * cellSize;
    
        this.shapeType = shapeType;
        this.clicked = false;
    
        this.scale.set(cellSize / this.width, cellSize / this.height);
        this.interactive = true;
        this.buttonMode = true;
    
        this.on('pointerdown', this.handler);    
    }
    setAnimationParameters(json_params) {
        this.animationParameters.to.x = json_params.to.x;
        this.animationParameters.to.y = json_params.to.y;
    }
    handler (){
        this.handleShapeClick(this);
    }
    select () {
        this.selected = true;
        this.tint = '0xAAAAAA';
    }
    unselect () {
        this.selected = false;
        this.tint = '0xFFFFFF';
    }
    checkIfNeighbourShape (otherShape) {
        return (Math.abs(this.xCoord-otherShape.xCoord)===1 ) && (this.yCoord===otherShape.yCoord) ||
               (Math.abs(this.yCoord-otherShape.yCoord)===1 && this.xCoord===otherShape.xCoord);
    }
    
};

class Match3Program {
    constructor() {
        this.update = this.update.bind(this);

        this.cells = [];
        this.matches = [];
    
        /**
         * Indicates how much cells we clear;
         */
        this.removedCells = 0;

        this.ShapeAndPoint = {
            shape: null,
            point: {x: null, y: null}
        };

        this.AnimationParameters = {
            shape1: null,
            shape2: null,
            enabled: false,
            animationFuncsArray: [],
            afterAnimationHandler: null,
            animateShapesAndPoints: []
        };
        this.points = 0;
        this.shouldAccountPoints = false;
        this.isControlEnabled = false;        
        
        this.Level = {};
        this.Level.ShapeTypes = [
            {name: 'star',points:5},
            {name: 'heart',points:5},
            {name: 'stone',points:5},
            {name: 'square',points:5},
            {name: 'emerald',points:5},
        ];
        this.Level.Field = {
            cellsByX: 6,
            cellsByY: 6,
            cellSize: 100
        };

        this.init();

        this.app = new PIXI.Application(700, 700, {backgroundColor: 0x1099bb});
        document.body.appendChild(this.app.view);

        this.field = new PIXI.Container();
        this.app.stage.addChild(this.field);
        this.field.selectedShape = null;


        this.pointsDisplay = new PIXI.Text("Points: " + this.points, {
            fontFamily: 'sans',
            fill: 'white',
            align: 'left'
        });
        this.app.stage.addChild(this.pointsDisplay);
        this.time = Date.now();
    }

    /**
     * This function moves array of shapes to their new points {x: Number, y: Number};
     */
    moveShapeToPoint(time) {
        let sum = 0;
        time /= 100;
        for(let i = 0; i < this.AnimationParameters.animateShapesAndPoints.length; i++)
        {
            sum = this.AnimationParameters.animateShapesAndPoints[i].point.x - this.AnimationParameters.animateShapesAndPoints[i].shape.x;
            sum += this.AnimationParameters.animateShapesAndPoints[i].point.y - this.AnimationParameters.animateShapesAndPoints[i].shape.y;
            if(sum > 2){
                this.AnimationParameters.animateShapesAndPoints[i].point.x += time * (this.AnimationParameters.animateShapesAndPoints[i].point.x - this.AnimationParameters.animateShapesAndPoints[i].shape.x);
                this.AnimationParameters.animateShapesAndPoints[i].point.y += time * (this.AnimationParameters.animateShapesAndPoints[i].point.y - this.AnimationParameters.animateShapesAndPoints[i].shape.y);
            } else {
                this.AnimationParameters.animateShapesAndPoints[i].shape.x = this.AnimationParameters.animateShapesAndPoints[i].point.x;
                this.AnimationParameters.animateShapesAndPoints[i].shape.y = this.AnimationParameters.animateShapesAndPoints[i].point.y;
                this.AnimationParameters.animateShapesAndPoints[i].point = null;
                this.AnimationParameters.animateShapesAndPoints[i].shape = null;
                this.AnimationParameters.animateShapesAndPoints.splice(i--, 1);
            }
        }
        /**
         *  this flag detects, that animation must be stopped
         */
        if(this.AnimationParameters.animateShapesAndPoints.length === 0)
            return false;
        else
            return true;
    }

    init() {
        this.timeoutToAdd = 0;
        this.timeoutToShift = 0;
        this.shouldShift = false;
        this.shouldAdd = false;

        this.preload();
    }
    preload() {
        PIXI.loader
            .add(this.getShapesFileNames())
            .load(this.create.bind(this));
    }
    create() {
        this.initField();
        this.drawField();
        this.resolveLines();
        this.shouldAccountPoints = true;
        this.update();
    }
    update() {
        this.app.renderer.render(this.app.stage);
        requestAnimationFrame(this.update);
        
        /**
         * There we start animate all currentAnimations;
         */
        this.lastTime = this.time;
        this.time = Date.now();
        if(this.AnimationParameters.enabled){
            for(let i = 0 ; i < this.AnimationParameters.animationFuncsArray.length; i++)
            {
                this.AnimationParameters.animationFuncsArray[i](this.time - this.lastTime);
            }
            // this.AnimationParameters.animationFuncsArray.forEach(function (func) {
            //     func(this.time - this.lastTime);
            // }.bind(this));
        }
        this.pointsDisplay.text = "Points: " + this.points;
    
        this.isControlEnabled = !(this.matches.length > 0);
    
        if(this.matches.length > 0)
        {
            this.removeMatches();
            this.shouldShift = true;
        }
        if (this.shouldShift)
        {
            this.timeoutToShift += 1;
            if(this.timeoutToShift > 10)
            {
                this.shiftToUp();
                this.shouldAdd = true;
                this.shouldShift = false;
                this.timeoutToShift = 0;
                this.findMatches();
            }
        }
        if (this.shouldAdd)
        {
            this.timeoutToAdd += 1;
            if(this.timeoutToAdd > 10)
            {
                this.addNewShapes();
                this.shouldAdd = false;
                this.timeoutToAdd = 0;
                this.findMatches();
            }
        }
    
        else
            this.initShapesCoordinates();
    
    
    }
    initField() {
        this.initFieldArray();
        this.populateFieldRandomShapes();
    }
    initFieldArray() {
        this.cells = new Array(this.Level.Field.cellsByX);
        for (let i = 0; i < this.Level.Field.cellsByX; i++) {
            this.cells[i] = [this.Level.Field.cellsByY];
            this.cells[i].emptyCellsCount =0;
        }
    }
    populateFieldRandomShapes () {
        for (let y = 0; y < this.Level.Field.cellsByY; y++) {
            for (let x = 0; x < this.Level.Field.cellsByX; x++) {    
                let shape = new Shape(this.getRandomShapeType(), x, y, this.Level.Field.cellSize, this.handleShapeClick.bind(this));
                this.field.addChild(shape);
                this.cells[x][y] = {};
                this.cells[x][y].shape = shape;
            }
        }
    }
    drawField() {

        this.field.x = (this.app.renderer.width - this.field.width) / 2;
        this.field.y = (this.app.renderer.height - this.field.height) / 2;

    }
    getShapesFileNames() {
        let shapesFileNames = [];
        for (let i = 0; i < this.Level.ShapeTypes.length; i++) {
            shapesFileNames.push("src/images/" + this.Level.ShapeTypes[i].name + ".png");
        }
        return shapesFileNames;
    }
    
    getRandomShapeType() {

        let r = this.getRandomInteger(0, this.Level.ShapeTypes.length - 1);
    
        return this.Level.ShapeTypes[r];
    }
        
    clearSelection() {

        for(let y = 0; y<this.Level.Field.cellsByY;y++)
        {
            for(let x = 0; x<this.Level.Field.cellsByX;x++)
            if(this.cells[x][y].shape!==null)
                this.cells[x][y].shape.unselect();
        }
        this.field.selectedShape = null;
    }
    getRandomInteger(min, max) {
        let rand = min + Math.random() * (max + 1 - min);
    
        rand = Math.floor(rand);
        return rand;
    }
    
    handleShapeClick (shape) {
        if(this.isControlEnabled ) {
            if (this.field.selectedShape === null) {
                shape.select();
                this.field.selectedShape = shape;
            }
            else if (this.field.selectedShape !== null) {

                if (shape.checkIfNeighbourShape(this.field.selectedShape)) {
                    this.swapShapes(shape, this.field.selectedShape, false);
                }    
                this.clearSelection();
            }
        }
    }

    swapShapes(shape1, shape2, test_swap)
    {

        this.cells[shape1.xCoord][shape1.yCoord].shape = shape2;
        this.cells[shape2.xCoord][shape2.yCoord].shape = shape1;

        let xCoord = shape1.xCoord;
        let yCoord = shape1.yCoord;
    
        shape1.setAnimationParameters({to: {x: shape2.x, y: shape2.y}});
        shape2.setAnimationParameters({to: {x: shape1.x, y: shape1.y}});

        shape1.xCoord = shape2.xCoord;
        shape1.yCoord = shape2.yCoord;
    
        shape2.xCoord = xCoord;
        shape2.yCoord = yCoord;

        this.AnimationParameters.shape1 = shape1;
        this.AnimationParameters.shape2 = shape2;
        this.AnimationParameters.enabled = true;
        if(!test_swap)
            this.AnimationParameters.afterAnimationHandler = this.afterAnimationSwapControl.bind(this);
        else
            this.AnimationParameters.afterAnimationHandler = null;            

        // now we start animation 
        this.AnimationParameters.animationFuncsArray.push(this.swapAnimation.bind(this));

        return false;

    }

    afterAnimationSwapControl(){

        this.findMatches();
        if (!(this.matches.length > 0)){
            this.swapShapes(this.AnimationParameters.shape1, this.AnimationParameters.shape2, true);
            return false;
        } else {
            return true;
        }

    }

    swapAnimation(time){

        let diff = Math.abs(this.AnimationParameters.shape1.x - this.AnimationParameters.shape1.animationParameters.to.x);
        diff += Math.abs(this.AnimationParameters.shape1.y - this.AnimationParameters.shape1.animationParameters.to.y);
        diff += Math.abs(this.AnimationParameters.shape2.x - this.AnimationParameters.shape2.animationParameters.to.x);
        diff += Math.abs(this.AnimationParameters.shape2.y - this.AnimationParameters.shape2.animationParameters.to.y);
        if(diff <= 2)
        {
            this.AnimationParameters.shape1.x = this.AnimationParameters.shape1.animationParameters.to.x;
            this.AnimationParameters.shape1.y = this.AnimationParameters.shape1.animationParameters.to.y;
            this.AnimationParameters.shape2.x = this.AnimationParameters.shape2.animationParameters.to.x;
            this.AnimationParameters.shape2.y = this.AnimationParameters.shape2.animationParameters.to.y;

            this.AnimationParameters.animationFuncsArray.splice(0,this.AnimationParameters.animationFuncsArray.length);        
            if(this.AnimationParameters.afterAnimationHandler)
                if(this.AnimationParameters.afterAnimationHandler()){
                    this.AnimationParameters.enabled = false;
                    this.AnimationParameters.shape1 = null;
                    this.AnimationParameters.shape2 = null;
                    this.AnimationParameters.animationFuncsArray.splice(0,this.AnimationParameters.animationFuncsArray.length);        
                }

            return;
        }
        time /= 100;
        this.AnimationParameters.shape1.x += time *  (this.AnimationParameters.shape1.animationParameters.to.x - this.AnimationParameters.shape1.x);
        this.AnimationParameters.shape1.y += time *  (this.AnimationParameters.shape1.animationParameters.to.y - this.AnimationParameters.shape1.y);

        this.AnimationParameters.shape2.x += time *  (this.AnimationParameters.shape2.animationParameters.to.x - this.AnimationParameters.shape2.x);
        this.AnimationParameters.shape2.y += time *  (this.AnimationParameters.shape2.animationParameters.to.y - this.AnimationParameters.shape2.y);
    }

    resolveLines() {

        this.findMatches();
    
        while (this.matches.length > 0 ) {
    
                this.removeMatches();
                this.fillEmptyCells();
                this.findMatches();
        }
    }
    findMatches() {

        this.matches = [];
    
        this.findHorizontalMatches();
    
        this.findVerticalMatches();
    
    }

    findHorizontalMatches() {

        let isHorizontal = true;
        for (let y=0; y<this.Level.Field.cellsByY; y++) {
    
            let matchLength = 1;
            for  (let x=0; x<this.Level.Field.cellsByX; x++) {
    
                if (x === this.Level.Field.cellsByX-1) {
                    this.addMatch(isHorizontal,matchLength, x,y);
                    matchLength = 1;
                }
                else {
    
                    if (this.cells[x][y].shape !== null &&
                        this.cells[x+1][y].shape !== null &&
                        this.cells[x][y].shape.shapeType === this.cells[x+1][y].shape.shapeType
                    ) {
    
                        matchLength += 1;
                    }else
                    {
    
                        this.addMatch(isHorizontal,matchLength, x,y);
                        matchLength = 1;
                    }
                }
    
            }
        }
    }
     findVerticalMatches() {
        let isHorizontal = false;
        for  (let x=0; x<this.Level.Field.cellsByX; x++) {
    
            let matchLength = 1;
            for ( let y=0; y<this.Level.Field.cellsByY; y++) {
    
                if (y === this.Level.Field.cellsByY-1) {
                    this.addMatch(isHorizontal,matchLength, x,y);
                    matchLength = 1;
                }
                else {
                    if (this.cells[x][y].shape !== null &&
                        this.cells[x][y+1].shape !== null &&
                        this.cells[x][y].shape.shapeType === this.cells[x][y+1].shape.shapeType
                    ) {
                        matchLength += 1;
                    }
                    else
                    {
                        this.addMatch(isHorizontal,matchLength, x,y);
                        matchLength = 1;
                    }
                }
    
            }
        }
    }
     removeMatches() {
    
        for (let i =0;i<this.matches.length;i++)
        {
            if(this.matches[i]!==null)
            {
                this.removeMatch(this.matches[i]);
            }
        }
        this.matches=[];
    
    
    }
     removeMatch(match) {
        this.removedCells = 0;
        if(match.isHorizontal){
    
            for(let j = 0;j<match.length;j++) {
                this.clearCell(match.startX-j,match.startY);
                this.removedCells++;
            }
        }
        else{
    
            for(let j = 0;j<match.length;j++) {
                this.clearCell(match.startX, match.startY - j);
                this.removedCells++;
            }
        }
    }
     clearCell(x,y) {
    
        if(this.cells[x][y].shape !==null) {
    
            if(this.shouldAccountPoints)
            {
                if(this.removedCells < 4)
                    this.points+=this.cells[x][y].shape.shapeType.points;
                else
                    this.points += (this.cells[x].emptyCellsCount - 2) * 5;
            }
            this.cells[x][y].shape.destroy();
            this.cells[x][y].shape = null;
            this.cells[x].emptyCellsCount+=1;
        }
    
    }
    
     fillEmptyCells(isInitial) {
        this.shiftToUp();
        this.addNewShapes();
    }

    shiftToBottom(){
    
        for  (let x=0; x<this.Level.Field.cellsByX; x++) {
            if( this.cells[x].emptyCellsCount>0)
                for ( let y=this.Level.Field.cellsByY; y>0; y--) {
                        this.shiftToBottomShape(x,y-1);
                }
        }
    
    
    }

    shiftToUp(){
    
        for  (let x=0; x<this.Level.Field.cellsByX; x++) {
            if( this.cells[x].emptyCellsCount>0)
                for ( let y=0; y<this.Level.Field.cellsByY-1; y++) {
                        this.shiftToUpShape(x,y+1);
                }
        }
    
    
    }

    shiftToBottomShape(x, y){
        while(y<this.Level.Field.cellsByY-1)
        {
            if(this.cells[x][y].shape!==null && this.cells[x][y+1].shape===null) {
    
                this.cells[x][y].shape.y += this.Level.Field.cellSize;
                this.cells[x][y].shape.yCoord++;
                this.cells[x][y+1].shape=this.cells[x][y].shape;
                this.cells[x][y].shape=null;
                console.log("shift", x,y);
    
            }
            y++;
        }
    }

    shiftToUpShape(x, y){
        while(y>0)
        {
            if(this.cells[x][y].shape !== null && this.cells[x][y-1].shape === null) {
    
                this.cells[x][y].shape.y -= this.Level.Field.cellSize;
                this.cells[x][y].shape.yCoord--;
                this.cells[x][y-1].shape=this.cells[x][y].shape;
                this.cells[x][y].shape=null;
                console.log("shift", x,y);
    
            }
            y--;
        }
    }

     addNewShapes() {
        for(let x=0; x<this.Level.Field.cellsByX; x++) {
            if(this.cells[x].emptyCellsCount>0)
                for ( let y=0; y<this.Level.Field.cellsByY; y++) {
                    if(this.cells[x][y].shape===null) {
                        let shape = new Shape(this.getRandomShapeType(), x, y, this.Level.Field.cellSize, this.handleShapeClick.bind(this));
                        this.field.addChild(shape);
                        this.cells[x][y] = {};
                        this.cells[x][y].shape = shape;
                    }
                }
        }
    }

     addMatch(horizontal, length, x, y){
        if(length>=3) {
            let match = {isHorizontal: horizontal, length: length, startX: x, startY: y};
            this.matches.push(match);
        }
    }
    
     initShapesCoordinates() {
        for(let y=0; y<this.Level.Field.cellsByY; y++) {
            for(let x=0; x<this.Level.Field.cellsByX; x++) {
                if (this.cells[x][y].shape!==null) {
                    this.cells[x][y].shape.xCoord =x;
                    this.cells[x][y].shape.yCoord =y;
                }
            }
        }
    }
                        
};
